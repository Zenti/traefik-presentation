---?image=traefik.png&position=50% 15%&size=auto 60%

@title[traefik]
@snap[south]
@size[2.5em](traefik)
@snapend

---

@title[Agenda]
## Agenda

- Was ist traefik?
- Was macht traefik besser als X/Y?
- Wie funktioniert traefik?
- Was macht traefik **nicht**?
- Demo

---

## Was ist traefik?

+++

@title[Reverse Proxy]
![Reverse Proxy](reverseproxydiagram.jpeg)
## Reverse Proxy

+++

@title[Load Balancer]
![Load Balancer](loadbalancingdiagram.png)
## Load Balancer

---

![nginx](nginx-logo.png)
![haproxy](haproxy-logo.png)

+++

![docker-everywhere](docker-everywhere.jpg)

+++

![future](creepy-future.jpeg)

+++

![manual-handling](manual-handling.gif)

---

@title[Was macht traefik besser?]
## Was macht traefik besser als nginx/HAProxy?

+++?image=architecture.png&size=auto auto

@title[Integration-Orchestrierung]

+++?image=docker-logo.png&size=auto 380px

@title[Docker]

+++?image=kubernetes-logo.png&size=auto auto
@title[Kubernetes]

+++?image=prometheus-logo.png&size=auto auto

@title[Metriken, Prometheus]

+++?image=lets-encrypt-logo.png&size=auto auto

@title[Let's Encrypt]

---

@title[Wie funktioniert traefik?]
## Wie funktioniert traefik?

+++?image=internal.png&size=auto auto

Note:

- Frontend
 - Regeln
 - Header an Client
- Backend
 - Sticky Sessions
 - Healthcheck
 - Load Balancer

+++?code=code/rules.txt&lang=ldif&title=Verfuegbare Regeln

+++?code=code/jira-docker-compose.yml&lang=yaml&title=Beispiel fuer docker-compose

@[7-14]
@[18]

+++

## [Doku und Infos](https://docs.traefik.io)

---

@title[Was macht traefik nicht?]
## Was macht traefik **nicht**?

+++

## Caching
- Issue auf GitHub: [#878](https://github.com/containous/traefik/issues/878)

---

![Automation](automation.gif)

---

# Demo-Zeit!
